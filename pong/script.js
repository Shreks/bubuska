let playArea = document.getElementById("playArea");

let rocket1X,rocket1Y;
let rocket2X,rocket2Y;
let rocketH,rocketW;

let ballX,ballY;
let ballHW;

let scoreLeft, scoreRight;

let keyCode, keyIsPressed;

let context;


function IntialValues()
{
    context =playArea.getContext("2d");
    scaleFactor = Math.sqrt(Math.pow(playArea.clientWidth,2)+Math.pow(playArea.height,2));

    rocketH = Math.floor(scaleFactor/5);
    rocketW = Math.floor(rocketH/8);
    rocket1X = 0;
    rocket1Y = playArea.height/2-rocketH/2;

    rocket2X = playArea.width-rocketW;
    rocket2Y = rocket1Y;

    ballHW = Math.floor(rocketW);
    ballX = Math.floor(playArea.width/2-ballHW/2)
    ballY = Math.floor(playArea.height/2-ballHW/2)
}

function RenderObjects()
{
   
    context.fillStyle ="darkcyan";
    context.fillRect(rocket1X,rocket1Y,rocketW,rocketH);
    context.fillRect(rocket2X,rocket2Y,rocketW,rocketH);
    context.fillRect(ballX,ballY,ballHW,ballHW);
}

function CalculatePhysics()
{
    if(keyIsPressed==true)
    {
        switch(keyCode)
        {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
        }
    }
    
    ballX++;
    ballY++;
}

function UpdateGame()
{
    CalculatePhysics();
    RenderObjects();
}

IntialValues();

setInterval(UpdateGame,10);